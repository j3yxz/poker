#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char *es_fgets(void){                                                   //si definisce la funzione che permette l'inserimento delle parole

     int dimStringhe=30;                                        //dimensione iniziale delle stringhe
    char *tmp=(char*)calloc(sizeof(char), dimStringhe);                 //si usa calloc per non avere memoria sporca, tmp è un puntatore a vettore
                                                                        //che verrà utilizzato per copiare un po' alla volta-
    char *dest=(char*)calloc(sizeof(char), dimStringhe+4);              //-la stringa dell'utente che andrà in dest che ha dimensione maggiore rispetto a tmp



        fgets(tmp, dimStringhe, stdin);                                 //l'utente inserisce la parola
        strcpy(dest, tmp);                                              //si prendono dimStringhe caratteri e si copiano in dest

        /*
            La fgets aggiunge automaticamente '\0' alla fine dei caratteri che legge da
            stdin che, quindi, sono al più dimStringhe-1 che vengono copiati in dest, quindi leggendo
            il carattere di posizione dimStringhe-2 se è diverso da '\0' significa che la stringa
            inserita dall'utente è più lunga (o è uguale a dimStringhe-2 , si controlla che sia !='\n'),
            in questo modo il ciclo perdura finchè lo stdin non si svuota completamente
        */

        while((*(tmp+dimStringhe-2) != '\0') && (*(tmp+dimStringhe-2) != '\n')){
            dimStringhe*=2;                                             //si raddoppia dimStringhe per gestire stringhe molto lunghe
            tmp=(char *)realloc(tmp, dimStringhe);                      //tmp viene ingrandita raddoppiando la dimensione
            if(tmp==NULL){                                              //controllo realloc()
                printf("Errore: memoria insufficiente\n");
                exit(1);
            }

            memset(tmp, 0, dimStringhe);                                //si pulisce tmp

            dest=(char *)realloc(dest, 2*dimStringhe);                  //si raddoppia la dimensione di dest
            if(dest==NULL){                                             //controllo realloc()
                printf("Errore: memoria insufficiente\n");
                exit(1);
            }

            fgets(tmp, dimStringhe, stdin);                             //fgets legge da stdin ciò che è rimasto in stdin,-
                                                                        //-non viene chiesto all'utente nuovo input perché lo-
                                                                        //-stdin è ancora in attesa e con il controllo !='\n'
                                                                        //si evita il rischio di chiedere l'input inutilmente

            strcat(dest, tmp);                                          //si copia il contenuto del nuovo tmp in coda a dest

        }
/* se non si vogliono gli spazi
        int j=0;
        while(!isspace(dest[j])){                                       //si controlla che l'utente non inserisca più di una parola
            j++;
        } */
        /*
        if(dest[j]!='\n'){
            printf("\nErrore: inserire una sola parola\n");
            exit(1);
        }
		*/
        if(dest[0]=='\n'){                                              //si controlla che l'utente non prema invio senza inserire nulla
            printf("\nErrore: inserire una parola\n");
            exit(1);
        }
        dest[strlen(dest)-1]='\0';										//metto il carattere di terminazione al posto di \n che mi troverei(opzionale ma non troppo)
        return dest;                                                    //la funzione restituisce il puntatore alla stringa completa
}



typedef struct Player {
	int punt;
	int puntRaggiunto;
	char *Giocatore;
} player;

/* Combina i due sottoarray ordinati usando la struttura player *
void merge(player *array,int p,int q,int r)
{	
	player scambio;
	for (;q<=r;q++){
		if(array[q].puntRaggiunto<array[q+1].puntRaggiunto) return;
		else {
			for(;q<=r;q++){
				for(int j=q; j>=p && array[j].puntRaggiunto>array[j+1].puntRaggiunto; j--){
					scambio=array[j];
					array[j]=array[j+1];
					array[j+1]=scambio;
					
				}
			}
		}
	}
}
*/
// Combina i due sottoarray ordinati [p, q] e [q+1, r]. quello della soluzione:
void merge(player *array, int p, int q, int r)
{
	int i = p;
	int j = q + 1;
	int k = 0; //array 0-based
	player* B =(player *) malloc((r-p+1)*sizeof(player));

	//Ordina
	while (i <= q && j <= r)
	{
		if (array[i].puntRaggiunto < array[j].puntRaggiunto)
			B[k] = array[i++];
		else
			B[k] = array[j++];

		k++;
	}
	//---

	//Ricopia leftover sx
	while (i <= q)
		B[k++] = array[i++];
	//---

	//Ricopia leftover dx
	while (j <= r)
		B[k++] = array[j++];
	//---

	//Ricopia B in A
	for (k = p; k <= r; k++)
		array[k] = B[k-p];
	//---

	free(B);
}

void mergeSort(player *data,int p,int r)
{
	int q = (p + r) / 2;           	// si noti che in C la divisione tra interi memorizzata in variabile intera comporta il troncamento del numero all'intero inferiore (floor)
	if (p < r)
	{
		mergeSort(data, p, q);    	// Ordina il sottoarray di sinistra
		mergeSort(data, q + 1, r);  // Ordina il sottoarray di destra
		merge(data, p, q, r);      	// Combina i due sottoarray
	}
}

int main(){

	player giocatore[4];
	
	char* scelta;
	int turno=1;
	puts("Inserire i nomi.\n");
	int numPlayers=4;

	for(int i=0;i<4;i++){
		if(i==3){
			puts("I giocatori sono 3? [y/n]");
			scelta=es_fgets();

			if(scelta[0]=='y' || scelta[0]=='Y' || scelta[0]=='s' || scelta[0]=='S'){
				
				numPlayers=3;
				break;
			}
		}

		printf("Inserisci il giocater %d\n",i+1 );
		(giocatore[i].Giocatore)=es_fgets();
	//	giocatore[i].indice_giocatore=i; questo l'ho tolto dalla struct
		giocatore[i].puntRaggiunto=0;
		giocatore[i].punt=0;
	}

	while(turno</*19*/2){

		//printf con numero di carte turno e giocatore che deve dare le carte
		printf("\n\nTurno: %d razdaiot karty %s\n",turno,giocatore[(turno-1)%numPlayers].Giocatore );
		if(turno<9){
			printf("Razdovati po [ %d ] kart\n",turno);
		} else if(turno>=9 &&turno <12){
			printf("Tiomnaia n° %d!!\n",turno-8);
		} else { 
			printf("Uje po [ %d ] kart\n",19-turno);
		}

		//faccio inserire i punteggi di ogni turno

		for( int i=0;i<numPlayers;i++){

			printf("Inserisci il punteggio di %s\n",giocatore[i].Giocatore);
			giocatore[i].punt=atoi(es_fgets());
			giocatore[i].puntRaggiunto+=giocatore[i].punt;
		}


		//stampo i giocatori e i punteggi

		for ( int i = 0; i < numPlayers; ++i)
		{
			printf("  %15s  ",giocatore[i].Giocatore );
		}

		puts("\n\n");
		for ( int i = 0; i < numPlayers; ++i)
		{
			printf("  %15d  ",giocatore[i].puntRaggiunto );
		}
		
		if (numPlayers==3) printf("\nRigiocare la partita? punteggi attuali: [%d] [%d] [%d]",giocatore[0].punt,giocatore[1].punt,giocatore[2].punt); //se 3 giocatori
		else printf("\nRigiocare la partita? punteggi attuali: [%d] [%d] [%d] [%d]",giocatore[0].punt,giocatore[1].punt,giocatore[2].punt,giocatore[3].punt);	//se 4 giocatori
		puts("\n[Y/n]");
		scelta=es_fgets();

		if(scelta[0]=='y' || scelta[0]=='Y' || scelta[0]=='s' || scelta[0]=='S'){
	
			turno--; 																//se la partita e' da rigiocare faccio fare
																					//un altro turno che però e' lo stesso e lo sovrascivo
			for (int i = 0; i < numPlayers; ++i)
			{
				giocatore[i].puntRaggiunto-=giocatore[i].punt;
			}
		}

		turno++;
	}


	mergeSort(giocatore,0,numPlayers-1);	//miraccomando nel mergeSort() bisogna passare gli argomenti da 0 a n-1 come nel for(;;)
	
	for( int i=0;i<numPlayers;i++){
		printf("  %15s  ",giocatore[i].Giocatore);

											//qua faccio l'arrotondamento per calcolare i prisidanie
		giocatore[i].puntRaggiunto+=50;
		giocatore[i].puntRaggiunto/=100;
	}
	puts("\n");
	for(int i=0; i < numPlayers; ++i){		//stampo i punteggi ottenuti
		printf("  %15d  ",giocatore[i].puntRaggiunto);
	}
	puts("\n");
											//visto che le struct sono ordinati il min è [0] e devo solo capire in quanti hanno perso
	int min=giocatore[0].puntRaggiunto;
	int pari=1;								//numero giocatori arrivati ultimi (di base 1, se di più lo si calcola adesso)
	int prisidanie=0;

											//calcolo pari, in modo che sia uguale al numero di giocatori arrivati ultimi a pari merito
	while(giocatore[++pari].puntRaggiunto==min && pari<numPlayers+1);	

	for(int i = numPlayers; i> pari;i--) {
		prisidanie+=giocatore[i-1].puntRaggiunto; 	//sommo i punteggi dei non pari
	}

	min*=pari;										//calcolo il valore dei perdenti da togliere al risultato
	prisidanie-=min;								//calcolo il valore effettivo dei prisidanie (vincitori-perdenti)
	
	if(pari==numPlayers) printf("Non ha vinto nessuno, che noia!");
	else {
		printf("Chi ha perso? ");
		for(int i = 0; i < pari; ++i) {
			printf("%s; ",giocatore[i].Giocatore);
		}
		
		if(prisidanie<=0) printf("\nBah si sono salvati e non devono fare prisidanie..");
		else	printf("\ne bisogna fare %d prisidanie!",prisidanie);
	}
	puts("\n");
	return 0; 
}