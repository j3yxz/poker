CFLAGS=-std=gnu99 -Wall -Wpedantic

all: Poker.exe

Poker.exe: pokerv2.o
	gcc -o Poker.exe ${CFLAGS} pokerv2.o

pokerv2.o: pokerv2.c
	gcc -c ${CFLAGS} pokerv2.c
